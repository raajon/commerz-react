import React from 'react';

const MenuItem = (props:Props) =>{

  return(
    <a href={props.menuItemData.link}>{props.menuItemData.link}</a>
  )
}

interface Props{
  menuItemData: MenuItemType;
}

export interface MenuItemType{
  label: string;
  link: string;
}

export default MenuItem;
