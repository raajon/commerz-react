import React from 'react';

const SubTitle = (props:Props) =>{

  return(
    <h3>{props.data}</h3>
  )
}

interface Props{
  data: string;
}

export default SubTitle;
