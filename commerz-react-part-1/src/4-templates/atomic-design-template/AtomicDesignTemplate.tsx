import React from 'react';
import PageHeader, { HeaderContentType } from '../../3-organisms/common/PageHeader';

const AtomicDesignTemplate = (props:Props) =>{

  return(
    <PageHeader headerContent={props.pageContent.pageHeader}/>
  )
}

interface Props{
  pageContent: PageContentType;
}

export interface PageContentType{
  pageHeader: HeaderContentType;
}

export default AtomicDesignTemplate;
