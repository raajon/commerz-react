import React from 'react';
import AtomicDesignTemplate, { PageContentType } from '../../4-templates/atomic-design-template/AtomicDesignTemplate';

const AtomicDesign = () =>{

  const page:PageContentType = {
    pageHeader:{
      title:"Hello World",
      subtitle:"Wellcome to react world!",
      menuContent:[
        {label:"link1", link:"link1"},
        {label:"link2", link:"link2"},
        {label:"link3", link:"link3"},
        {label:"link4", link:"link4"}
      ]
    }
  }

  return(
    <AtomicDesignTemplate pageContent={page}/>
  )
}

export default AtomicDesign;
