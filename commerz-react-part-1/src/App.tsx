import React from 'react';
import './App.css';
import AtomicDesign from './5-pages/atomic-design/AtomicDesign'

function App() {
  return (
    <div className="App">
      <AtomicDesign/>
    </div>
  );
}

export default App;
