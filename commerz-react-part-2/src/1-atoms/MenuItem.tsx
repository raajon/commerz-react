import React from 'react';
import { Link } from "react-router-dom";

const MenuItem = (props:Props) =>{

  return(
    <Link to={props.menuItemData.link}>{props.menuItemData.link}</Link>
  )
}

interface Props{
  menuItemData: MenuItemType;
}

export interface MenuItemType{
  label: string;
  link: string;
}

export default MenuItem;
