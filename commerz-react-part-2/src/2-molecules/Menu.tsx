import React from 'react';
import MenuItem, { MenuItemType } from '../1-atoms/MenuItem';

const Menu = (props:Props) =>{
console.log(props.menuContent.length )
  return(
    <>
      {props.menuContent.map((mc, i) => <span key={i}><MenuItem menuItemData={mc}/> {(props.menuContent.length > i+1) ? " | " : ""} </span>)}
    </>
  )
}

interface Props{
  menuContent: MenuItemType[];
}

export default Menu;
