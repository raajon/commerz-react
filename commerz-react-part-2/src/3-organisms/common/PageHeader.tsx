import React from 'react';
import TitleWithSubTitle from '../../2-molecules/TitleWithSubTitle';
import Menu from '../../2-molecules/Menu';
import Line from '../../1-atoms/Line';
import { MenuItemType } from '../../1-atoms/MenuItem';

const PageHeader = (props:Props) =>{

  return(
    <>
      <TitleWithSubTitle title={props.headerContent.title} subtitle={props.headerContent.subtitle}/>
      <Menu menuContent={props.headerContent.menuContent}/>
      <Line/>
    </>
  )
}

interface Props{
  headerContent: HeaderContentType;
}

export interface HeaderContentType{
  title: string;
  subtitle: string;
  menuContent: MenuItemType[];
}

export default PageHeader;
