import React from 'react';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import './App.css';
import AtomicDesign from './5-pages/atomic-design/AtomicDesign'
import Page2 from './5-pages/page2/Page2'
import Layout from './5-pages/layout/Layout'

function App() {

  const createElement = (element:any) =>{
    return (
      <Layout>
        {element}
      </Layout>
    )
  }

  const router = createBrowserRouter([
    { path: "/", element: createElement(<AtomicDesign/>) },
    { path: "/page1", element: createElement(<AtomicDesign/>) },
    { path: "/page2", element: createElement(<Page2/>) },
  ]);


  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
