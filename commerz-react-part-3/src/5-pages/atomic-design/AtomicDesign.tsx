import React from 'react';
import AtomicDesignTemplate from '../../4-templates/atomic-design-template/AtomicDesignTemplate';

const AtomicDesign = () =>{

  const text = "Page1";

  return(
    <>
      <AtomicDesignTemplate text={text}/>
    </>
  )
}

export default AtomicDesign;
