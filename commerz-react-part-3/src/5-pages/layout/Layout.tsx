import React from 'react';
import { Outlet } from "react-router-dom";
import PageHeader, { HeaderContentType } from '../../3-organisms/common/PageHeader';

const AtomicDesign = (props:Props) =>{

  console.log(props)

  const pageHeader:HeaderContentType = {
    title:"Hello World",
    subtitle:"Wellcome to react world!",
    menuContent:[
      {label:"page1", link:"/page1"},
      {label:"page2", link:"/page2"},
      {label:"page2+name", link:"/page2/dupa"},
      {label:"page4", link:"/page4"}
    ]
  }

  return(
    <>
      <PageHeader headerContent={pageHeader}/>
      <div>
        <Outlet/>
      </div>
    </>
  )
}

interface Props{
  // children:any;
}

export default AtomicDesign;
