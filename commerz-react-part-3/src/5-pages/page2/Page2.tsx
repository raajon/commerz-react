import React from 'react';
import { useLoaderData,  } from "react-router-dom";
import AtomicDesignTemplate from '../../4-templates/atomic-design-template/AtomicDesignTemplate';

//@types/react-router-dom RouteComponentProps
// export const loader = async({params}:any) =>{
export const loader = async({params, request}:any) =>{
  return params;
}

const Page2 = () =>{
  const params:Params = useLoaderData() as Params;

  const text = "Hello " + params.name;
  return(
    <>
      <AtomicDesignTemplate text={text}/>
    </>
  )
}

interface Params{
  name:string;
}

export default Page2;
