import React from 'react';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import './App.css';
import AtomicDesign from './5-pages/atomic-design/AtomicDesign'
import Page2, { loader as page2Loader,} from './5-pages/page2/Page2'
import Layout from './5-pages/layout/Layout'

function App() {

  const router = createBrowserRouter([
    { path: "/", element:  <Layout/>, children:[
      { path: "/", element: <AtomicDesign/> },
      { path: "/page1", element: <AtomicDesign/> },
      { path: "/page2", element: <Page2/> },
      { path: "/page2/:name", element: <Page2/>, loader:page2Loader }
    ]}
  ]);


  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
