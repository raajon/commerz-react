import React from 'react';

const Title = (props:Props) =>{

  return(
    <h1>{props.data}</h1>
  )
}

interface Props{
  data: string;
}

export default Title;
