import React from 'react';
import Title from '../1-atoms/Title';
import SubTitle from '../1-atoms/SubTitle';

const TitleWithSubTitle = (props:Props) =>{

  return(
    <>
      <Title data={props.title}/>
      <SubTitle data={props.subtitle}/>
    </>
  )
}

interface Props{
  title:string;
  subtitle:string;
}

export default TitleWithSubTitle;
