import axios from 'axios';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { save } from '../../6-redux/employeeSlice';

const Page1 = () =>{
  const dispatch = useDispatch();
  const employeeData:any = useSelector((state:any) => state.employeeData.employeeData);

  const searchHandler = () =>{
    axios
      .get("http://localhost:3001/employees")
      .then(data => dispatch(save(data.data)))
      .catch(error => console.log(error));
  }

  return(
    <>
        <button onClick={searchHandler}>Search</button>
        {employeeData.data.map((e:any,i:number)=>
          <div key={i}>
            <Link to={"/page2/"+e.id}>
              {e.first_name} {e.last_name}
            </Link>
          </div>
        )}
    </>
  )
}

interface Params{
  name:string;
}

export default Page1;
