import axios from 'axios';
import React, { useState } from 'react';
import { Link, useLoaderData,  } from "react-router-dom";

export const loader = async({params, request}:any) =>{
  const data = await axios.get("http://localhost:3001/employees/"+params.id);
  return data.data;
}

const Page2 = () =>{
  const [employee, setEmployee] = useState<any>(useLoaderData())

  const update = () =>{
    const newEmployee = {...employee};
    newEmployee.gender = "Female";
    axios.put("http://localhost:3001/employees/"+employee.id, newEmployee)
      .then(data=>setEmployee(data.data))
  }

  return(
    <>
      <pre>{JSON.stringify(employee, null, 2)}</pre>
      <Link to="/page1">back</Link>
      <button onClick={update}>update</button>
    </>
  )
}

export default Page2;
