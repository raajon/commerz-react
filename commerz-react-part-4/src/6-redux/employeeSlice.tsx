import { createSlice } from '@reduxjs/toolkit';

let employeeDataInit:any = {
  data:[]
}

export const employeeSlice = createSlice({
  name: 'questData',
  initialState: {
    employeeData: employeeDataInit,
  },
  reducers: {
    save: ({employeeData}, {payload}) =>{
      employeeData.data = payload;
    }
  },
})

export const {
  save
} = employeeSlice.actions

export default employeeSlice.reducer
