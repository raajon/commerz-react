import axios from 'axios';
import React from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import * as actions from '../../6-redux/actions/index';
import {Employee, RootState} from '../../6-redux/Types';


const Page1 = (props:any) =>{
  // const dispatch = useDispatch();
  // const employeeData:any = useSelector((state:any) => state.employeeData.employeeData);

  const searchHandler = () =>{
    axios
      .get("http://localhost:3001/employees")
      .then(data => props.save(data.data))
      .catch(error => console.log(error));
  }
console.log(props.employeeData)
  return(
    <>
        <button onClick={searchHandler}>Search</button>
        {props.employeeData.employee?.map((e:any,i:number)=>
          <div key={i}>
            <Link to={"/page2/"+e.id}>
              {e.first_name} {e.last_name}
            </Link>
          </div>
        )}
    </>
  )
}

interface Params{
  name:string;
  employeeData:Employee[];
  save:Function;
}


const mapStateToProps = (state:any) => {
    return {
      employeeData: state.employee
    };
};

const mapDispatchToProps = (dispatch:any) => {
    return {
      save: (employee:Employee[]) =>dispatch(actions.saveEmployee(employee))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1);
