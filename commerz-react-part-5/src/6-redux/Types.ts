export interface Employee{
  if:number;
  first_name:string;
  last_name:string;
  email:string;
  gender:string;
  status:string;
}

export interface RootState{
  employee: Employee[];
}
