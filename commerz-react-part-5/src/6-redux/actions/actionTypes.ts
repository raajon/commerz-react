import { Employee } from "../Types";

export const SAVE_EMPLOYEE = 'SAVE_EMPLOYEE';

export interface ISaveEmployee{
  type: typeof SAVE_EMPLOYEE;
  payload: Employee[];
}
