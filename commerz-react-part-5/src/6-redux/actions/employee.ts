import { Employee } from '../Types';
import * as ActionTypes from './actionTypes';

export const saveEmployee = (employee: Employee[]):ActionTypes.ISaveEmployee =>{
  return {
    type: ActionTypes.SAVE_EMPLOYEE,
    payload:employee
  }
}
