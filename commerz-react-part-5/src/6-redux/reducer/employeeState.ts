import * as actionTypes from '../actions/actionTypes';
import {RootState} from '../Types'

const initialState:RootState={
  employee:[]
}

const reducer = (state:RootState=initialState, action:actionTypes.ISaveEmployee) =>{
  switch(action.type){
    case actionTypes.SAVE_EMPLOYEE: return {...state, employee:action.payload};
    default: return state;
  }
}

export default reducer;
