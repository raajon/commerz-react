
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import employeeReducer from './reducer/employeeState'

//
// const logger = store =>{
//   return (next: (arg0: any) => any): (arg0: any) => any => {
//     return action =>{
//       // console.log("[middleware]", action);
//       const result = next(action);
//       // console.log("[middleware]", store.getState());
//       return result;
//     }
//   }
// }

const rootReducer = combineReducers({
  employee: employeeReducer
})
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(rootReducer, applyMiddleware(thunk));
