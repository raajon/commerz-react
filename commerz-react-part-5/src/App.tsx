import React from 'react';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import './App.css';
import Layout from './5-pages/layout/Layout'
import Page1 from './5-pages/page1/Page1';
import Page2, { loader as page2Loader,} from './5-pages/page2/Page2'

function App() {

  const router = createBrowserRouter([
    { path: "/", element:  <Layout/>, children:[
      { path: "/", element: <Page1/> },
      { path: "/page1", element: <Page1/> },
      { path: "/page2", element: <Page2/> },
      { path: "/page2/:id", element: <Page2/>, loader:page2Loader }
    ]}
  ]);


  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
