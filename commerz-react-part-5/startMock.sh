#!/usr/bin/env bash

set -o errexit
set -o pipefail

SCRIPT_DIR="$(cd `dirname ${0}` && pwd)"

ESC=$(printf '\033')
BLUE="${ESC}[0;34m"
GREEN="${ESC}[0;32m"
CYAN="${ESC}[0;36m"
NC="${ESC}[0m" # No Color

trap "trap - SIGTERM && kill -- -$$ 2>/dev/null" EXIT

cd "${SCRIPT_DIR}/"
npm run start 2>&1 | sed -e "s/^/${CYAN}frontend:     |${NC}  /" &
cd "${SCRIPT_DIR}/mock-api/"
npx json-server -p3001 --watch src/db.json 2>&1 | sed -e "s/^/${GREEN}mock-server:  |${NC}  /"
