import React from 'react';

const Header = (props:Props) => {


  return (
    <h1>
      {props.header}
    </h1>
  );
}

interface Props{
  header: string;
}


export default Header;
