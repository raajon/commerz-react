import React from 'react';

const Subheader = (props:Props) => {
  return (
    <h2>
      {props.subheader}
    </h2>
  );
}

const Subsubheader = (props:Props) => {
  return (
    <h3>
      {props.subheader}
    </h3>
  );
}

const Fun2 = () =>{
  console.log("test");
}

interface Props{
  subheader: string;
}


export {Subheader, Subsubheader, Fun2};
