import React from 'react';
import Header from '../1-atoms/Header';
import {Subheader, Subsubheader} from '../1-atoms/Subheader';

const FullHeader = (props:FullHeaderType) => {


  return (
    <>
      <Header header={props.header}/>
      <Subheader subheader={props.subheader}/>
    </>
  );
}

export interface FullHeaderType{
  header: string;
  subheader:string;
}


export default FullHeader;
