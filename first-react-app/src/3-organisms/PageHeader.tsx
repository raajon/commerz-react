import React from 'react';
import FullHeader, {FullHeaderType} from '../2-molecules/FullHeader';
import Line from '../1-atoms/Line'

const PageHeader = (props:Props) => {

  const fullHeaderData = {
    header: "header",
    subheader:"subheader"
  }

  return (
    <>
      <FullHeader header={fullHeaderData.header} subheader={fullHeaderData.subheader}/>
      <Line/>
    </>
  );
}

interface Props{
  fullHeader: FullHeaderType
}


export default PageHeader;
