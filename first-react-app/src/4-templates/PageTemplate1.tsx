import React from 'react';
import {FullHeaderType} from '../2-molecules/FullHeader';
import PageHeader from '../3-organisms/PageHeader';

import FullHeader from '../2-molecules/FullHeader';

const PageTemplate1 = (props: Props) => {

  return (
    <>
      <PageHeader fullHeader={props.content.fullHeader}/>
      {props.content.textContent}
    </>
  );
}

interface Props{
  content: PageTemplateType
}

export interface PageTemplateType{
  fullHeader: FullHeaderType
  textContent: string;
}

export default PageTemplate1;
