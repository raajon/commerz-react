import React from 'react';
import PageTemplate1, {PageTemplateType} from '../4-templates/PageTemplate1'
import PageHeader from '../3-organisms/PageHeader';

import FullHeader from '../2-molecules/FullHeader';

const Page1 = () => {

  const contentData:PageTemplateType = {
    fullHeader:{
      header: "header",
      subheader: "subheader"
    },
    textContent:"Page1"
  };

  return (
    <PageTemplate1 content={contentData}/>
  );
}

export default Page1;
