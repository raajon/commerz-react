import React from 'react';
import Page1 from './5-pages/Page1'
import './App.css';


const App = () => {

  return (
    <div className="App">
      <Page1/>
    </div>
  );
}

export default App;
